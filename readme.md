# this is the title

content: hello world!

Causality:
Root-unapt configuration querying  optimization 
(lacking secondary index caching)
-> Due to no secondary index caching it will cost more time to query with secondary index.(cause a slowdown) 
-> That is because only if submitting query using secondary index, the secondary index data can be read from disk.
(The secondary index is store as a hidden table, i.e. a hidden CF.)
(CF -> ColumnFamily, 2I CF -> secondary index ColumnFamily)
 
Symptom:
Query with secondary index will cost more time than expected.
 
Fix:
Scheme-enable feature-add/using cache
(enable 2I CF caching, considering the situation of the parent CF caching.)
•	If the parent CF has key caching enable, enable key caching on the secondary index CF. Otherwise leave the 2I CF at Caching.NONE.
•	The same time, if the parent CF has row caching enable, and if the row number of secondary index CF is bigger than its average column number (i.e. the 2I CF is high cardinality), enable row caching on the 2I CF. Otherwise, do nothing.
 
Fix Version/s:
1.1.1
 
https://issues.apache.org/jira/secure/attachment/12525682/4197-1.1.txt
 
